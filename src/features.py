import pandas as pd
from scipy import stats
import numpy as np

import config


def create_statistical_features(data, numerical_cols):
    """Creates statistical features for numerical columns."""
    for col in numerical_cols:
        data[col + '_mean'] = data[col].mean()
        data[col + '_std'] = data[col].std()
        data[col + '_min'] = data[col].min()
        data[col + '_max'] = data[col].max()
        data[col + '_skew'] = data[col].skew()
        data[col + '_kurt'] = data[col].kurt()
        data[col + '_median'] = data[col].median()
        data[col + '_var'] = data[col].var()
        data[col + '_iqr'] = data[col].quantile(0.75) - data[col].quantile(0.25)
        data[col + '_cv'] = data[col].std() / data[col].mean()
    return data


def create_categorical_features(data, categorical_cols):
    """Creates frequency and entropy features for categorical columns."""
    for col in categorical_cols:
        data[col + '_freq'] = data[col].value_counts()
        data[col + '_mode'] = data[col].mode()[0]
        data[col + '_entropy'] = stats.entropy(data[col].value_counts())
    return data


def remove_outliers(data, numerical_cols, z_score_threshold):
    """Removes outliers based on z-scores."""
    z_scores = stats.zscore(data[numerical_cols])
    data = data[(np.abs(z_scores) < z_score_threshold).all(axis=1)]
    return data


def remove_correlated_features(data, numerical_cols, correlation_threshold):
    """Removes highly correlated features."""
    corr_matrix = data[numerical_cols].corr()
    corr_features = set()
    for i in range(len(corr_matrix.columns)):
        for j in range(i):
            if abs(corr_matrix.iloc[i, j]) > correlation_threshold:
                colname = corr_matrix.columns[i] if corr_matrix.columns[i] not in corr_features else corr_matrix.columns[j]
                corr_features.add(colname)
    data.drop(corr_features, axis=1, inplace=True)
    return data