# File paths
RAW_DATA_PATH = "data/raw/synthetic_network_traffic.csv"
PROCESSED_DATA_PATH = "data/processed/preprocessed_data.csv"
MODEL_PATH = "data/models/anomaly_detection_model.pkl"  # Adjust file extension as needed

# Data preprocessing options
NUMERICAL_COLS = ['SourcePort', 'DestinationPort', 'BytesSent', 'BytesReceived', 'PacketsSent', 'PacketsReceived', 'Duration']
CATEGORICAL_COLS = ['SourceIP', 'DestinationIP', 'Protocol']
IMPUTATION_STRATEGY = 'median'  # Options: 'mean', 'median', 'most_frequent'
ENCODING_METHOD = 'label_encoding'  # Options: 'label_encoding', 'onehot_encoding'

# Feature engineering options
Z_SCORE_THRESHOLD = 3  # Threshold for outlier removal
CORRELATION_THRESHOLD = 0.8  # Threshold for removing correlated features

# Model training options
TEST_SIZE = 0.2
RANDOM_STATE = 42
MODEL_TYPE = 'svm'  # Options: 'svm', 'random_forest', 'xgboost', etc.

# SVM-specific hyperparameters (adjust if using SVM)
SVM_KERNEL = 'rbf' 
SVM_C = 1.0
SVM_GAMMA = 'scale'

# Add other model-specific hyperparameters as needed