from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from xgboost import XGBClassifier
import pickle

import config


def train_model(X_train, y_train, model_type):
    """Trains a model based on the specified model type."""
    if model_type == 'svm':
        model = SVC(kernel=config.SVM_KERNEL, C=config.SVM_C, gamma=config.SVM_GAMMA)
    elif model_type == 'random_forest':
        model = RandomForestClassifier(n_estimators=100, random_state=config.RANDOM_STATE)
    elif model_type == 'xgboost':
        model = XGBClassifier(random_state=config.RANDOM_STATE)
    else:
        raise ValueError("Invalid model type:", model_type)

    model.fit(X_train, y_train)
    return model


def predict(model, X_test):
    """Makes predictions on the test data."""
    y_pred = model.predict(X_test)
    return y_pred


def save_model(model, model_path):
    """Saves the trained model to a file."""
    with open(model_path, 'wb') as f:
        pickle.dump(model, f)


def load_model(model_path):
    """Loads a saved model from a file."""
    with open(model_path, 'rb') as f:
        model = pickle.load(f)
    return model

