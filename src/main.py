from src import utils, features, model
import config
from sklearn.metrics import accuracy_score, f1_score


def main():
    """Main function to orchestrate the anomaly detection workflow."""
    # Load data
    data = utils.load_data(config.RAW_DATA_PATH)

    # Preprocess data
    processed_data = utils.preprocess_data(data.copy(), 
                                         config.NUMERICAL_COLS, 
                                         config.CATEGORICAL_COLS, 
                                         config.IMPUTATION_STRATEGY, 
                                         config.ENCODING_METHOD)

    # Feature engineering
    processed_data = features.create_statistical_features(processed_data, config.NUMERICAL_COLS)
    processed_data = features.create_categorical_features(processed_data, config.CATEGORICAL_COLS)
    processed_data = features.remove_outliers(processed_data, config.NUMERICAL_COLS, config.Z_SCORE_THRESHOLD)
    processed_data = features.remove_correlated_features(processed_data, config.NUMERICAL_COLS, config.CORRELATION_THRESHOLD)

    # Prepare data for training
    X = processed_data.drop('IsAnomaly', axis=1)
    y = processed_data['IsAnomaly']
    X_train, X_test, y_train, y_test = utils.split_data(X, y, config.TEST_SIZE, config.RANDOM_STATE)

    # Train model
    trained_model = model.train_model(X_train, y_train, config.MODEL_TYPE)

    # Make predictions
    y_pred = model.predict(trained_model, X_test)

    # Evaluate model
    accuracy = accuracy_score(y_test, y_pred)
    f1 = f1_score(y_test, y_pred)
    print("Accuracy:", accuracy)
    print("F1 Score:", f1)

    # ... (You can add more evaluation metrics and visualizations as needed)


if __name__ == "__main__":
    main()