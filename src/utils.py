import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, OneHotEncoder, StandardScaler
from sklearn.impute import SimpleImputer
from scipy import stats
import numpy as np

import config


def load_data(data_path):
    """Loads the data from the specified file path."""
    return pd.read_csv(data_path)


def preprocess_data(data, numerical_cols, categorical_cols, imputation_strategy, encoding_method):
    """Preprocesses the data with imputation, encoding, and scaling."""
    # Imputation
    imputer = SimpleImputer(strategy=imputation_strategy)
    data[numerical_cols] = imputer.fit_transform(data[numerical_cols])

    # Encoding
    if encoding_method == 'label_encoding':
        encoder = LabelEncoder()
        for col in categorical_cols:
            data[col] = encoder.fit_transform(data[col])
    elif encoding_method == 'onehot_encoding':
        encoder = OneHotEncoder(handle_unknown='ignore', sparse=False)
        encoded_data = encoder.fit_transform(data[categorical_cols])
        encoded_df = pd.DataFrame(encoded_data, columns=encoder.get_feature_names_out(categorical_cols))
        data = pd.concat([data, encoded_df], axis=1)
        data = data.drop(categorical_cols, axis=1)
    else:
        raise ValueError("Invalid encoding method:", encoding_method)

    # Scaling
    scaler = StandardScaler()
    data[numerical_cols] = scaler.fit_transform(data[numerical_cols])
    data.to_csv('data/preprocessed/preprocessed_data.csv', index=False)
    return data


def split_data(X, y, test_size, random_state):
    """Splits the data into training and testing sets."""
    return train_test_split(X, y, test_size=test_size, random_state=random_state)

