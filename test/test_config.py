import pytest
from src import config


def test_file_paths():
    """Checks if file paths are valid strings."""
    assert isinstance(config.RAW_DATA_PATH, str)
    assert isinstance(config.PROCESSED_DATA_PATH, str)
    assert isinstance(config.MODEL_PATH, str)


def test_data_preprocessing_options():
    """Verifies data preprocessing options."""
    assert isinstance(config.NUMERICAL_COLS, list)
    assert isinstance(config.CATEGORICAL_COLS, list)
    assert config.IMPUTATION_STRATEGY in ["mean", "median", "most_frequent"]
    assert config.ENCODING_METHOD in ["label_encoding", "onehot_encoding"]


def test_feature_engineering_options():
    """Ensures feature engineering thresholds are numeric."""
    assert isinstance(config.Z_SCORE_THRESHOLD, (int, float))
    assert isinstance(config.CORRELATION_THRESHOLD, (int, float))


def test_model_training_options():
    """Validates model training settings."""
    assert 0 <= config.TEST_SIZE <= 1
    assert isinstance(config.RANDOM_STATE, int)
    assert config.MODEL_TYPE in [
        "svm",
        "random_forest",
        "xgboost",
    ]

def test_svm_hyperparameters():
    """Checks SVM hyperparameters if SVM is selected."""
    if config.MODEL_TYPE == "svm":
        assert config.SVM_KERNEL in ["linear", "poly", "rbf", "sigmoid"]
        assert isinstance(config.SVM_C, (int, float))
        assert config.SVM_GAMMA in ["scale", "auto"] or isinstance(config.SVM_GAMMA, (int, float))