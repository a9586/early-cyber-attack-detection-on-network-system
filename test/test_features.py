import pytest
import pandas as pd
from src import features, config


@pytest.fixture
def sample_data():
    # Assuming the preprocessed data is in 'sample_preprocessed.csv'
    data = pd.read_csv("sample_preprocessed.csv")
    return data


def test_calculate_statistical_features(sample_data):
    """Tests the creation of statistical features."""
    data_with_stats = features.calculate_statistical_features(
        sample_data, ["BytesSent", "PacketsReceived"]
    )  # Test on multiple numerical columns
    assert set(data_with_stats.columns) >= {
        "BytesSent",
        "PacketsReceived",
        "BytesSent_mean",
        "BytesSent_std",
        "PacketsReceived_mean",
        "PacketsReceived_std",
        # ... add other expected statistical features based on your implementation
    }


def test_derive_categorical_features(sample_data):
    """Tests the creation of features from categorical columns."""
    data_with_cat_features = features.derive_categorical_features(
        sample_data, ["SourceIP", "Protocol"]
    )
    assert set(data_with_cat_features.columns) >= {
        "SourceIP",
        "Protocol",
        "SourceIP_freq",
        "SourceIP_mode",
        "Protocol_freq",
        "Protocol_mode",
        # ... add other expected categorical features
    }


def test_select_features(sample_data):
    """Tests feature selection based on predefined criteria."""
    selected_data = features.select_features(sample_data)
    assert set(selected_data.columns) <= set(sample_data.columns)  # Features are removed
    # Add assertions to check if specific features are included or excluded
    # based on your feature selection logic and criteria