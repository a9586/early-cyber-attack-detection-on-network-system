import pytest
import pandas as pd
from src import model, config


@pytest.fixture
def sample_data():
    # Assuming the preprocessed and split data is available
    X_train = pd.read_csv('sample_preprocessed_train.csv')
    X_test = pd.read_csv('sample_preprocessed_test.csv')
    y_train = pd.read_csv('sample_preprocessed_train_labels.csv')
    y_test = pd.read_csv('sample_preprocessed_test_labels.csv')
    return X_train, X_test, y_train, y_test


def test_train_model(sample_data):
    """Tests the model training process."""
    X_train, _, y_train, _ = sample_data
    model.train_model(X_train, y_train)  # Assuming 'model' is your trained model object
    # Add assertions to check the type of the trained model, training time, etc.


def test_evaluate_model(sample_data):
    """Tests the evaluation of the trained model."""
    _, X_test, _, y_test = sample_data
    accuracy, precision, recall, f1 = model.evaluate_model(X_test, y_test)
    # Assert that the evaluation metrics are numeric and within expected ranges
    assert isinstance(accuracy, float)
    assert 0 <= accuracy <= 1
    # Repeat similar assertions for precision, recall, and f1


def test_save_model(sample_data):
    """Tests the saving of the trained model."""
    model.train_model(*sample_data[:2])  # Train on the first two data splits
    model.save_model(config.MODEL_PATH)  # Assume config.MODEL_PATH is the save path
    # Add assertions to check if the file exists and has non-zero size