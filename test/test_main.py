import os
import pytest
from src import main, config


def test_main(mocker):
    """Tests the main workflow of the project."""
    # Mock the data loading, preprocessing, and model training functions
    mocker.patch('src.main.load_data', return_value=pd.DataFrame())
    mocker.patch('src.main.preprocess_data', return_value=pd.DataFrame())
    mocker.patch('src.main.train_model')
    # Assert that the model was saved to the specified path
    assert os.path.isfile(config.MODEL_PATH)