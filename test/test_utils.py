import pytest
import pandas as pd
from src import utils, config


@pytest.fixture
def sample_data():
    # Assuming the preprocessed data is in 'sample_preprocessed.csv'
    data = pd.read_csv("sample_preprocessed.csv")
    return data


def test_load_data():
    """Tests if data is loaded correctly as a Pandas DataFrame."""
    data = utils.load_data(config.PROCESSED_DATA_PATH)  # Use PROCESSED_DATA_PATH
    assert isinstance(data, pd.DataFrame)


def test_preprocess_data_already_processed(sample_data):
    """Tests handling of already preprocessed data."""
    processed_data = utils.preprocess_data(
        sample_data.copy(),
        config.NUMERICAL_COLS,
        config.CATEGORICAL_COLS,
        config.IMPUTATION_STRATEGY,
        config.ENCODING_METHOD,
    )
    # Check if the data remains unchanged after processing
    assert processed_data.equals(sample_data)


def test_split_data(sample_data):
    """Verifies data splitting into training and testing sets."""
    X = sample_data.drop("IsAnomaly", axis=1)  # Assuming 'IsAnomaly' is the target
    y = sample_data["IsAnomaly"]
    X_train, X_test, y_train, y_test = utils.split_data(
        X, y, config.TEST_SIZE, config.RANDOM_STATE
    )
    assert len(X_train) + len(X_test) == len(X)
    assert len(y_train) + len(y_test) == len(y)